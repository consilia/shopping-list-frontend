import Products from './components/Products'
import Header from './components/Header'
import EditProduct from './components/EditProduct'
import AddProduct from './components/AddProduct'
import { useState, useEffect } from "react"
import Modal, { setAppElement } from 'react-modal'
import axios from '../src/axios.js'

Modal.setAppElement('#root');


const App = () => {
  const [products, setProducts] = useState([])
  async function fetchProducts() { 
      const res = await axios.get('/products')
      const downloadedProducts = res.data
      setProducts(downloadedProducts)
  } 
  useEffect(() => {
      fetchProducts();
  })
  const [isOpen, setIsOpen] = useState(false)
  const [editedProduct, setEditedProduct] = useState({})

  async function addProduct(product) {
      try{
          const res = await axios.post('/products', {product})
          const newProduct = res.data
          setProducts([...products, newProduct])
      }
      catch (err){
          console.log(err)
      }
  }

async function editProduct(product) {
    const index = products.findIndex(x => x._id === product.id)
    await axios.put('/products/' + product.id, {product})

    if(index >= 0){
        products[index] = product;
        setProducts(products)
    }
    toggleModal()
}
  const toggleModal = () => {
      setIsOpen(!isOpen)
  }
  const openEditProduct = (product) => {
      toggleModal();
      setEditedProduct(product)
  }

  async function deleteProduct(id){
      await axios.delete('/products/' + id)
      setProducts(products.filter((product) => product._id !== id))
  }

  return (
    <div className='App'>
      <Header title='Lista zakupów'/>
      <AddProduct onAdd={addProduct}/>

      {products.length > 0 ? <Products products={products} onDelete={deleteProduct} onEdit={openEditProduct}/> : <p>Brak produktów</p>}

      <Modal contentLabel='Edytuj zadanie' isOpen={isOpen} >
            <EditProduct editedProduct={editedProduct} onEdit={editProduct} onToggle={toggleModal}></EditProduct>
      </Modal>
    </div>
  )
}

export default App
