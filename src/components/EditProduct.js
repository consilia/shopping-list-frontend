import { useState } from "react"

const EditProduct = ({editedProduct, onEdit, onToggle}) => {
    const [name, setName] = useState(editedProduct.name)
    const [category, setCategory] = useState(editedProduct.category)
    const [quantity, setQuantity] = useState(editedProduct.quantity)

    const edit = (e) => {
        e.preventDefault()

        const id = editedProduct._id
        onEdit({id, name, category, quantity})
    }
    return (
        <form className='edit-list' onSubmit={edit}>
            <div className='input-div'>
                <label>Produkt</label>
                <input type='text' placeholder={name} value={name} onChange={(e) => setName(e.target.value)} required/>
            </div>
            <div className='input-div'>
                <label>Kategoria</label>
                <select value={category} onChange={(e) => setCategory(e.target.value)}>
                    <option value='fruits&vagetables'>Owoce i warzywa</option>
                    <option value='dairy'>Nabiał</option>
                    <option value='bread'>Pieczywo</option>
                </select>
            </div>
            <div className='input-div'>
                <label>Ilość</label>
                <input type='number' placeholder={quantity} value={quantity} min="0" onChange={(e) => setQuantity(e.target.value)}/>
            </div>
            <div className='buttons'>
                <input type='submit' className='btn' value='Zapisz'/>
                <button onClick={onToggle} className='btn'>Anuluj</button>
            </div>
        </form>
    )
}

export default EditProduct
