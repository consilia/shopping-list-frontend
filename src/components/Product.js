import { useState } from "react"
import { FiX } from "react-icons/fi";
import { FiEdit } from "react-icons/fi";
import { GiMilkCarton } from 'react-icons/gi'
import { GiSlicedBread } from 'react-icons/gi'
import { GiShinyApple } from 'react-icons/gi'

const Product = ({product, onDelete, onEdit}) => {
    const [isChecked, setIsChecked] = useState(false)

    return (
        <div className='product'>
            {product.category === 'dairy' && <GiMilkCarton className='category'></GiMilkCarton> }
            {product.category === 'fruits&vagetables' && <GiShinyApple className='category'></GiShinyApple> }
            {product.category === 'bread' && <GiSlicedBread className='category'></GiSlicedBread> }
            <div className='product-info'>
                <div className='input-name'>
                    <input type='checkbox' value={isChecked} onChange={(e) => setIsChecked(!isChecked)} required/>
                    <p className={isChecked ? 'checked' : ''}>{product.name}</p>
                </div>
                <p>{product.quantity}</p>
            </div>
            <div className='product-icons'>
                <FiEdit onClick={() => onEdit(product)}></FiEdit>
                <FiX onClick={() => onDelete(product._id)}></FiX>
            </div>
        </div>
    )
}

export default Product



