import Product from './Product'

const Products = ({products, onDelete, onEdit}) => {

    return(
        <div className='products'>
            {products.map((product) => (
                <Product key={product._id} product={product} onDelete={onDelete} onEdit={onEdit}/>
            ))}
        </div>
    )
}

export default Products



